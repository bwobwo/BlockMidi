package bwo.blockmidi.synth;

import javax.sound.midi.Soundbank;

public class SoundfontEntry {
	String name;
	Soundbank bank;
	int id;
	
	public SoundfontEntry(String name, Soundbank bank, int id){
		this.name=name;
		this.bank=bank;
		this.id=id;
	}
	
	public boolean equals(SoundfontEntry oth){
		return this.name.equals(oth.name)&&this.id==oth.id;
	}
}
