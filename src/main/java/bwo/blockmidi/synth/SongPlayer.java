package bwo.blockmidi.synth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * Represents a single playing song. 
 * 
 * @author BwoBwo
 */
public class SongPlayer {
	public static Map<BlockPos,SongPlayer> players = new HashMap<BlockPos,SongPlayer>();
	
	static void disposeAll(){
		for(SongPlayer p: players.values()){
			p.dispose();
		}
		players.clear();
	}
	
	public static void update(float dt){
		for(Iterator<Map.Entry<BlockPos, SongPlayer>> it = 
			players.entrySet().iterator();it.hasNext();) {
			SongPlayer pl = it.next().getValue();
			if(pl.tick(dt)){
				pl.dispose();
				it.remove();
			}
		}
	}
	
	public static float globalVolume=1;
	public boolean[] settings;
	public List<Cursor> activeCursors = new ArrayList<Cursor>();
	
	//Should these always be loaded per-song? 
	public List<SynthWrapper> loadedSynths = new ArrayList<SynthWrapper>();

	public List<TickTimer> timers = new ArrayList<TickTimer>();
	
	public World world;
	public BlockPos startPos;
	
	int allowedActionsCount,allowedActionsReset;
	int curActions=0;
	float actionsTimer;
	
	
	public void addAction() throws Exception{
		this.curActions++;
		if(this.curActions>this.allowedActionsCount){
			throw new Exception("Song event overflow");
		}
	}
	
	public SongPlayer(World world, BlockPos startPos,int settingsSize,int allowedActionsCount,int allowedActionsReset){		
		this.settings = new boolean[settingsSize];	
		this.allowedActionsCount = allowedActionsCount;
		this.allowedActionsReset = allowedActionsReset;
		this.world=world;
		this.startPos=startPos;
		
		new Cursor(this,this.world,this.startPos);
		if(players.containsKey(startPos)){
			players.get(startPos).dispose();
		}
		
		players.put(startPos,this);
	}
	
	public boolean tick(float dt){
		this.actionsTimer+=dt;
		if(this.actionsTimer>=this.allowedActionsReset){
			this.curActions=0;
			this.actionsTimer=0;
		}
		
		try{		
			for(int i=0;i<this.activeCursors.size();i++){
				Cursor c = this.activeCursors.get(i);
				if(c.update()){
					this.activeCursors.remove(i);
					i--;
				}
			}
			if(this.activeCursors.size()>0) return false;
		} catch(Exception e){}
		return true;
	}

	public void dispose(){
		for(SynthWrapper wr: this.loadedSynths){
			wr.close();
		}
	}
}