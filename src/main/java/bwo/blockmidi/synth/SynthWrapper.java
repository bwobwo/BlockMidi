package bwo.blockmidi.synth;

import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Soundbank;
import javax.sound.midi.Synthesizer;

import bwo.blockmidi.core.BMUtil;

/**
 * 
 * Wraps a single {@link Synthesizer}
 * It keeps single loaded soundbank and a map of pressed keyStates
 * 
 * @see #getFreeChannel(Soundbank, int)
 * 
 * @author BwoBwo
 */
public class SynthWrapper {
	public static final byte ATTACK = 73;
	public static final byte DECAY = 75;
	public static final byte VOLUME = 7;
	
	private Synthesizer synth;
	
	boolean[] pressedStates = new boolean[16];
	Soundbank bank;
	
	public SynthWrapper(Soundbank bank) throws MidiUnavailableException{
		this.synth = MidiSystem.getSynthesizer();
		this.synth.open();
		
		if(bank!=null){
			this.synth.unloadAllInstruments(synth.getDefaultSoundbank());
			this.synth.loadAllInstruments(bank);			
		}
		this.bank=bank;
	}
	
	public void close(){
		for(MidiChannel chn: this.synth.getChannels()){
			chn.allNotesOff();
		}
		this.synth.close();		
	}
	
	public boolean isOpen(){
		return this.synth.isOpen();
	}

	public String soundfont(){
		return this.bank.getName();
	}

	public void noteDown(int channel,int key,Cursor cursor,float sleepTime){
		key = BMUtil.clamp(key, 0, 126);
		channel = BMUtil.clamp(channel, 0, 15);
				
		synth.getChannels()[channel].controlChange(ATTACK,cursor.attack);
		synth.getChannels()[channel].controlChange(DECAY, cursor.decay);
		synth.getChannels()[channel].controlChange(VOLUME,(byte)
				(cursor.volume*SongPlayer.globalVolume));
		synth.getChannels()[channel].noteOn(key, cursor.velocity);
		this.pressedStates[channel] = true;
		final int _channel = channel;
		final int _key = key;
		new TickTimer(sleepTime){
			@Override
			public void fire() {	
				noteUp(_channel,_key);
			}
		};		
	}
	
	public void noteUp(int channel, int key){
		key = BMUtil.clamp(key, 0, 126);
		channel = BMUtil.clamp(key, 0, 15);
		this.synth.getChannels()[channel].noteOff(key);
		this.pressedStates[channel] = false;
	}
	
	/**
	 * Finds a midi channel that does not currently press noteNumber.
	 * @param bank - Has to match this synths loaded bank. 
	 * @param noteNumber
	 * @return The matching channel or -1 if none were found. 
	 */
	public int getFreeChannel(Soundbank bank,int noteNumber){
		if(this.bank!=bank) return -1;
		for(int i=0;i<this.pressedStates.length;i++){
			if(this.pressedStates[i]) continue;
			return i;
		}
		return -1;
	}
}
