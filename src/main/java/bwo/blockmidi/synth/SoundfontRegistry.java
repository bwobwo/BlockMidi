package bwo.blockmidi.synth;
import java.util.HashSet;

import javax.sound.midi.Soundbank;


public class SoundfontRegistry extends HashSet<SoundfontEntry>{
	private static final long serialVersionUID = -4614437031230331880L;

	public void add(String name,int numeric ,Soundbank font){
		this.add(new SoundfontEntry(name,font,numeric));
	}
}
