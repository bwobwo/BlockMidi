package bwo.blockmidi.synth;

public class SmallId {
	static int running=0;
	
	int id;
	public SmallId(){
		this.id=running;
		running++;
	}
	
	public String toString(){
		return String.format("%04d", this.id);
	}
}
