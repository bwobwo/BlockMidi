package bwo.blockmidi.synth;

import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Soundbank;

import bwo.blockmidi.blocks.InstrumentBlock;
import bwo.blockmidi.blocks.InstrumentBlock.InstrumentInventoryTE;
import bwo.blockmidi.blocks.InstrumentItem;
import bwo.blockmidi.core.BMBlockBase;
import bwo.blockmidi.core.BMClient;
import bwo.blockmidi.core.BMUtil;
import bwo.blockmidi.core.NBTSettings;
import bwo.blockmidi.core.BMDataBlock.MusicTE;
import bwo.blockmidi.blocks.NoteBlock;
import bwo.blockmidi.blocks.SettingsBlock;
import bwo.blockmidi.blocks.StartBlock;
import bwo.blockmidi.blocks.TimeSignature;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLog;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * Represents a single cursor instance in a loaded {@link SongPlayer}. 
 * @author BwoBwo
 */
public class Cursor {
	public World world;
	public BlockPos position;
	public BlockPos lastPos;
	public SongPlayer player;
	
	public byte velocity=127,attack=0,decay=0,release=0,volume=127;
	public byte bank=0,program=0;
	
	public int bpm = 100;
	
	public float beatUnit = 4; // 'numerator'
	public float beatsPerBar = 4; // 'denominator' 
	
	public int toneOffset=0;
	public int octaveOffset=0;
	
	public Soundbank soundfont=null;
	
	TickTimer timer;
	SmallId id;
	public Cursor(){	
	}
	
	public void debug(String msg){
		System.out.println(this.id+": "+msg);
	}
	
	public Cursor(SongPlayer player,World w, BlockPos p){
		this.id = new SmallId();
		this.player=player;
		this.world=w;
		this.position=p;
		this.player.activeCursors.add(this);
	}
	
	public void sleepMs(float ms){
		debug("Sleeping "+ms);
		final Cursor c = this;
		this.timer = new TickTimer(ms){
			@Override
			public void fire() {				
				c.debug("Waking up from "+ms);
				c.timer=null;
			}
		};
	}
		
	public float beatToMs(float beats){
		return beats/this.bpm*60*1000;
	}
	
	public float barToMs(float bars){
		return this.beatToMs(bars*this.beatsPerBar);
	}
	
	
	//Notes are usually expressed as 'whole', 'half','quarters' etc
	//so you express them as rationals.
	public float noteToMs(int noteNum,int noteDen){
		//'how many whole notes are in a bar'
		float barSize = (float)this.beatUnit/(float)this.beatsPerBar;
		
		//'how big is the passed note'
		float noteSize = (float)noteNum/(float)noteDen; // '1/4' note = quarter note. 
		return this.barToMs(noteSize*barSize);
	}
	
	public static boolean isValidCell(World world,BlockPos pos){
		IBlockState state = world.getBlockState(pos);
		if(state.getBlock() instanceof BlockLog) return true;	
		return (state.getBlock() instanceof BMBlockBase 
			&& !(state.getBlock() instanceof StartBlock));
	}
	
	public void playNoteAt(int note, float sleep) throws MidiUnavailableException{
		//Find a free wrapper and send a noteDown event.
		for(SynthWrapper wrapper: this.player.loadedSynths){
			int ch = wrapper.getFreeChannel(this.soundfont, note);
			if(ch==-1) continue;
			wrapper.noteDown(ch, note, this,sleep);
			return;
		}
		
		//No free synths, request a new one. 
		SynthWrapper nu = new SynthWrapper(this.soundfont);
		this.player.loadedSynths.add(nu);
		nu.noteDown(0, note, this,sleep);
	}
	
	/**
	 * This is where the magic happens.
	 * @param pos
	 * @throws Exception 
	 */
	public void enterCell(BlockPos pos) throws Exception{
		debug("Entering "+pos);
		this.lastPos=this.position;
		this.position=pos;
		Block block = this.world.getBlockState(pos).getBlock();		
		TileEntity te = this.world.getTileEntity(pos);
		NBTSettings stn=null;
		
		if(te instanceof MusicTE){
			 stn = ((MusicTE)te).getData();
		}
		
		if(block instanceof NoteBlock){
			String sleepType = stn.getEnum("sleepMode");
			byte note = stn.getByte("note");
			boolean pass = stn.getBool("pass");
			
			float sleep = stn.getFloat("sleep");
			
			
			switch(sleepType){
				case "Notes":
					int full = Float.floatToIntBits(sleep);
					int beats = full>>16;
					int bars = full&16;
					sleep = this.noteToMs(beats, bars);
					break;
				case "Beats":
					sleep = this.beatToMs(sleep);
					break;
				case "Bars":
					sleep= this.barToMs(sleep);
					break;
			}
			
			this.playNoteAt(note, sleep);
			
			if(!pass){
				this.sleepMs(sleep);
			}
			return;
		}
		
		if(block instanceof SettingsBlock){
			byte atk = stn.getByte("attack");
			byte decay = stn.getByte("decay");
			byte velocity = stn.getByte("velocity");
			this.attack = atk<=-1?this.attack:atk;
			this.decay = decay<=-1?this.decay:decay;
			this.velocity = velocity<=-1?this.velocity:velocity;
			return;
		}
		
		if(block instanceof TimeSignature){
			byte beats = stn.getByte("beats");
			byte bars = stn.getByte("bars");
			int bpm = stn.getInt("bpm");
			this.beatUnit = beats<=-1?this.beatUnit:beats;
			this.beatsPerBar = bars<=-1?this.beatsPerBar:bars;
			this.bpm = bpm<=-1?this.bpm:bpm;
			return;
		}		
		
		if(te instanceof InstrumentInventoryTE){
			InstrumentInventoryTE invTE = (InstrumentInventoryTE)te;
			ItemStack stack = invTE.getItem();
			if(!(stack.getItem() instanceof InstrumentItem)) return;
			
			int sf = InstrumentItem.getSoundfont(stack);
			Soundbank f = SoundfontManager.getSoundbank(sf,BMClient.clientSoundbankMap);
			if(f==null) return; 
			
			this.soundfont = SoundfontManager.getSoundbank(sf,BMClient.clientSoundbankMap);
			this.bank = (byte) InstrumentItem.getBank(stack);
			this.program = (byte) InstrumentItem.getProgram(stack);
			return;
		}
		
		this.update();
	}
	
	public boolean update() throws Exception{
		if(this.timer!=null) return false;
		this.player.addAction();
		
		//1. Find all neighbours.
		List<BlockPos> neighbours = new ArrayList<BlockPos>();
		for(BlockPos p: BMUtil.NEIGHBOURS){
			BlockPos pos = this.position.add(p);
			if(pos.equals(this.lastPos)) continue;			
			if(isValidCell(this.world,pos)){
				neighbours.add(pos);
			}
		}
		
		if(neighbours.size()==0){
			return true;
		}
		
		//2. Enter all neighbours, creating clones if necessary. 
		for(int i=0;i<neighbours.size();i++){
			if(i==neighbours.size()-1){
				this.enterCell(neighbours.get(i));
				break;
			}
			this.clone().enterCell(neighbours.get(i));
		}
		return false;
	}
	
	/**
	 * This is what's called when multiple neighbouring music blocks are found. 
	 * @param newPos
	 * @return
	 */
	public Cursor clone(){
		Cursor c = new Cursor(this.player,this.world,this.position);
		debug("cloning "+c.id.toString());
		c.velocity = this.velocity;
		c.attack=this.attack;
		c.decay=this.decay;
		c.release=this.release;
		c.volume=this.volume;
		c.soundfont=this.soundfont;
		c.bank=this.bank;
		c.program=this.program;
		c.lastPos=this.position;
		c.bpm = this.bpm;
		c.beatUnit = this.beatUnit;
		c.beatsPerBar = this.beatsPerBar;
		c.toneOffset = this.toneOffset;
		c.octaveOffset = this.octaveOffset;
		return c;
	}
}
