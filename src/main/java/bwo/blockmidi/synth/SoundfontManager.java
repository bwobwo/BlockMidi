package bwo.blockmidi.synth;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sound.midi.Soundbank;

import bwo.blockmidi.core.BMNetwork;
import bwo.blockmidi.network.SFSync;

/**
 * The central loader and provider of {@link SoundFont}s. 
 * @author BwoBwo
 */
public class SoundfontManager {
	//At their core, soundbanks are defined by their filename.
	static Map<String,Soundbank> loaded = new HashMap<String,Soundbank>();
	
	public static IDirProvider provider;
	public static final String SEPARATOR = "\n";
	public static final String REGISTRY_FILENAME = "registry.dat";

	public static void setDirectoryProvider(IDirProvider newProvider){
		provider=newProvider;
	}

	public static List<String> load(boolean loadToMemory) throws IOException{
			loadSoundbanks(loadToMemory);
			List<String> ls = loadRegistry();
			BMNetwork.sendToAll(new SFSync(ls));
			return ls;
	}
	
	/**
	 * Loads all valid sf2/dls soundfonts found under Minecraft/soundfonts
	 * Called from {@link #load()}, but I might start calling it directly clientside.
	 * @param loadToMidi
	 * @throws IOException
	 */
	private static void loadSoundbanks(boolean loadToMidi) throws IOException{

	}
	
	/**
	 * Loads or creates the registry file and adds new found entries.
	 * 
	 * @return
	 * @throws IOException
	 */
	private static List<String> loadRegistry() throws IOException{
		List<String> registry = new ArrayList<String>();		

		//Load registry file (create if missing)
		File registryFile = new File(provider.getSaveDir(),REGISTRY_FILENAME);
		registryFile.createNewFile();

		String in = new String(Files.readAllBytes(registryFile.toPath()));
		if(!in.equals("")){
			String[] rows = in.split(SEPARATOR);
			for(String s: rows){
				registry.add(s);
			}
		}

		//Add new entries
		int s1 = registry.size();
		for(String loadedSoundbank: loaded.keySet()){
			if(!registry.contains(loadedSoundbank)) registry.add(loadedSoundbank);
		}

		//if registry changed, save to file. 
		if(registry.size()!=s1){
			Files.write(registryFile.toPath(), 
			String.join(SEPARATOR,registry).getBytes(),StandardOpenOption.WRITE);			
		}	
		return registry;
	}

	
	public static Soundbank getSoundbank(int id, List<String> registry){
		return loaded.get(registry.get(id));
	}

	public static List<Soundbank> getAllSoundbanks(List<String> orderMap){
		List<Soundbank> soundbanks=new ArrayList<Soundbank>();
		for(String s: orderMap){
			soundbanks.add(loaded.get(s));
		}
		return soundbanks;
	}
}
