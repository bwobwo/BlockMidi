package bwo.blockmidi.synth;

import java.io.File;

public interface IDirProvider {
	File getSoundfontDirInt();
	File getSaveDirInt();

	public default File getSoundfontDir(){
		File f = this.getSoundfontDirInt();
		f.mkdirs();
		return f;
	}
	
	public default File getSaveDir(){
		File f = this.getSaveDirInt();
		f.mkdirs();
		return f;
	}
}
