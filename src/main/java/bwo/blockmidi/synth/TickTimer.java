package bwo.blockmidi.synth;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Simple timer that's not threaded, instead call its {@link #tick(float)} with your delta. 
 * @author BwoBwo
 */
public abstract class TickTimer {
	public static List<TickTimer> timers = new ArrayList<TickTimer>();
	public static void update(float dt){
		for(Iterator<TickTimer> iter = timers.iterator();iter.hasNext();){
			if(iter.next().tick(dt))iter.remove();
		}
	}
	float curTime,waitTime;
	public TickTimer(float waitTime){
		this.waitTime=waitTime;
		this.curTime=0;
		timers.add(this);
	}
	public boolean tick(float dt){
		curTime+=dt;
		if(curTime>=waitTime){		
			this.fire();
			return true;
		}
		return false;
	}
	public abstract void fire();
}
