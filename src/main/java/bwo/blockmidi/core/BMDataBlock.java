package bwo.blockmidi.core;

import bwo.blockmidi.network.MBlockSync;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.EnumPacketDirection;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * The base class for most active music blocks. 
 * Includes a configurable Tile Entity to easily make settings blocks. 
 * @author BwoBwo
 */
public abstract class BMDataBlock extends BMBlockBase implements ITileEntityProvider{
	static{
		BMRegistryHandler.registerTileEntity("musicte",MusicTE.class);
	}	
	
	
	public abstract Object makeGui(World world, BlockPos pos,EntityPlayer player);

	public NBTPrototype data;
	protected abstract NBTPrototype getDataTemplate(NBTPrototype blank);
	
	public BMDataBlock(String name){
		super(name);	
		this.data = this.getDataTemplate(new NBTPrototype());
		this.data.setBlockType(""+getRegistryName());
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new MusicTE(this.data);
	}

	public static class MusicTE extends TileEntity{
		NBTSettings savedata;
		
		public NBTSettings getData(){
			return savedata;
		}
		
		public MusicTE(){
			this.savedata = new NBTSettings(this,new NBTPrototype());
		}
		
		public MusicTE(NBTPrototype prototype){
			this.savedata = new NBTSettings(this,prototype);
		}
		
		public void sendUpdate(){
			BMNetwork.sendToServer(new MBlockSync(pos,writeToNBT(new NBTTagCompound())));
		}
		
		@Override
		public void readFromNBT(NBTTagCompound compound){
			super.readFromNBT(compound);
			if(!compound.hasKey("block_type_id")) return;
			String typeId = compound.getString("block_type_id");
			Block b = Block.getBlockFromName(typeId);			
			if(!(b instanceof BMDataBlock)) return;
			NBTPrototype prototype = ((BMDataBlock)b).data;
			this.savedata = new NBTSettings(this,prototype);
			this.savedata.read(compound);
		}

		@Override
		public NBTTagCompound writeToNBT(NBTTagCompound compound){	
			super.writeToNBT(compound);
			if(this.savedata.getBlockType()==null) return compound;
			this.savedata.write(compound);
			compound.setString("block_type_id",this.savedata.getBlockType());
			return compound;
		}

		@Override
		public NBTTagCompound getUpdateTag()
		{
			NBTTagCompound compound = super.getUpdateTag();					
			this.savedata.write(compound);
			return compound;
		}
		
		@Override
		public SPacketUpdateTileEntity getUpdatePacket()
		{
			return new SPacketUpdateTileEntity(pos, getBlockMetadata(), getUpdateTag());
		}
		
		@Override
		public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt)
		{
			//If i add checks here on the player I could probably just... sync here without the packet?		
			if(net.getDirection() == EnumPacketDirection.CLIENTBOUND)
			{
				readFromNBT(pkt.getNbtCompound());
			}
		}
		
		public void clientUpdate(NBTTagCompound compound){
			this.savedata.read(compound);		
			this.markDirty();
			IBlockState st = this.getWorld().getBlockState(this.getPos());
			this.getWorld().notifyBlockUpdate(this.getPos(), st, st, 3);
		}
	}
}