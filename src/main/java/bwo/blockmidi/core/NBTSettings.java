package bwo.blockmidi.core;

import java.nio.charset.StandardCharsets;

import bwo.blockmidi.core.BMDataBlock.MusicTE;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;

/**
 * NBT wrapper that only allows edits conforming to a 'prototype'
 * @author BwoBwo
 */
public class NBTSettings{
	private NBTPrototype prototype;
	private NBTTagCompound self = new NBTTagCompound();
	private MusicTE entity;	
	
	public NBTSettings(MusicTE entity,NBTPrototype prototype){
		this.entity=entity;
		this.prototype=prototype;
	}
	
	public void sendUpdate(){
		entity.sendUpdate();
	}
	
	private boolean checkInt(String k, Object v){
		if(this.prototype==null) return true;
		NBTBase b = this.prototype.getTag(k);
		switch(b.getId()){
			case 0:
				return v == null;
			case 1:
				return v instanceof Byte || v instanceof Boolean;
			case 2:
				return v instanceof Short;
			case 3:
				return v instanceof Integer;
			case 4:
				return v instanceof Long;
			case 5:	
				return v instanceof Float;
			case 6:
				return v instanceof Double;
			default:
				return false;
		}
	}
	
	private void check(String k, Object v){
		if(k.equals("block_type_id")) throw new RuntimeException("Disallowed key: "+k);
		if(!checkInt(k,v)) throw new RuntimeException("Type mismatch: "+v.getClass()+" is not "+this.prototype.getTag(k).getClass());
	}
	
	public String getEnum(String k){
		if(!self.hasKey(k)) return prototype.getEnum(k);
		return prototype.getEnum(k,this.getByte(k));
	}
	
	public String[] getEnumValues(String k){
		return this.prototype.getEnumValues(k);
	}
		
	public boolean getBool(String k){
		if(!self.hasKey(k)) return prototype.getBoolean(k);
		return self.getBoolean(k);		
	}
	public String getString(String k){
		if(!self.hasKey(k)) return prototype.getString(k);
		return self.getString(k);
	}
	public byte getByte(String k){
		if(!self.hasKey(k)) return prototype.getByte(k);
		return self.getByte(k);
	}
	public short getShort(String k){
		if(!self.hasKey(k)) return prototype.getShort(k);
		return self.getShort(k);
	}
	public int getInt(String k){
		if(!self.hasKey(k)) return prototype.getInteger(k);
		return self.getInteger(k);
	}
	public long getLong(String k){
		if(!self.hasKey(k)) return prototype.getLong(k);
		return self.getLong(k);
	}
	public float getFloat(String k){
		if(!self.hasKey(k)) return prototype.getFloat(k);
		return self.getFloat(k);
	}
	public double getDouble(String k){
		if(!self.hasKey(k)) return prototype.getDouble(k);
		return self.getDouble(k);
	}
		
	public void setByte(String k, double b){
		this.check(k, (byte)(int)b);
		self.setByte(k, (byte)(int)b);
	}
	
	public void setEnum(String k, String v){
		self.setByte(k,this.prototype.enumIndex(k, v));
	}
	
	public void setBoolean(String k,boolean b){
		this.check(k,b);
		self.setBoolean(k,b);
	}
	public void setString(String k,String v){
		this.check(k, v);
		self.setString(k,v);
	}
	public void setInt(String k, double v){
		this.check(k, (int)v);
		self.setInteger(k, (int)v);
	}
	public void setShort(String k, short v){
		this.check(k, v);
		self.setShort(k,v);
	}
	public void setLong(String k,long v){
		this.check(k,v);
		self.setLong(k,v);
	}
	public void setFloat(String k, float v){
		this.check(k, v);
		self.setFloat(k,v);
	}
	public void setDouble(String k, double d){
		this.check(k,d);
		self.setDouble(k,d);
	}
	public void fromBytes(ByteBuf buf){
		self = new NBTTagCompound();
		int c = buf.readByte();
		for(int i=0;i<c;i++){
			String key = buf.readCharSequence(
				buf.readByte(), StandardCharsets.UTF_8).toString();
			int id = buf.readByte();
			switch(id){
				case 1:
					this.setBoolean(key,buf.readBoolean());
					break;
				case 2:
					setShort(key,buf.readShort());
					break;
				case 3:
					setInt(key,buf.readInt());
					break;
				case 4:
					setLong(key, buf.readInt());
					break;
				case 5:
					setFloat(key, buf.readFloat());
					break;
				case 6:
					setDouble(key, buf.readDouble());
					break;
				case 8:
					setString(key, buf.readCharSequence(
						buf.readByte(),StandardCharsets.UTF_8).toString());
			}
		}
	}	
	
	public void write(NBTTagCompound cpd){
		for(String s: self.getKeySet()){
			cpd.setTag(s, self.getTag(s));
		}
	}
	
	public void read(NBTTagCompound cpd){
		for(String s: cpd.getKeySet()){			
			if(!prototype.hasKey(s)) continue;
			if(!prototype.getTag(s).getClass().equals(
					cpd.getTag(s).getClass())) continue;
			self.setTag(s, cpd.getTag(s));
		}
	}
	
	public String toString(){
		return this.self.toString();
	}

	public String getBlockType() {
		return this.prototype.getBlockType();
	}
}
