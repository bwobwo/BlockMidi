package bwo.blockmidi.core;

import java.util.ArrayList;
import java.util.List;

import bwo.blockmidi.network.SFSync;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

/**
 * Wrapper around the {@link SimpleNetworkWrapper}. 
 * 
 * Seems like you could actually interface with netty directly
 * so this class can be rewritten to use less reflection and packet headers.
 * @author BwoBwo
 *
 */
public class BMNetwork {
	public static SimpleNetworkWrapper network;
	
	public static void init(){
		network = NetworkRegistry.INSTANCE.newSimpleChannel("blockmidi");
	}
	
	
	public static List<Class<? extends BMMessage>> registeredMessages = 
		new ArrayList<Class<? extends BMMessage>>();
	
	public static void register(BMMessage messageObj,
		boolean serverHandler, boolean clientHandler){
			register(messageObj.getClass(),serverHandler,clientHandler);
	}

	public static void register(Class<? extends BMMessage> messageClass, 
		boolean serverHandler, boolean clientHandler){
			registeredMessages.add(messageClass);
			if(serverHandler){
				network.registerMessage(BMMessage.MessageHandlerOverride.class, 
						BMMessage.MessageOverride.class, 0, Side.SERVER);			
			}
			if(clientHandler){
				network.registerMessage(BMMessage.MessageHandlerOverride.class, 
						BMMessage.MessageOverride.class, 0, Side.CLIENT);						
			}
	}
	
	
	public static void sendToServer(BMMessage base){
		network.sendToServer(new BMMessage.MessageOverride(base));
	}
	
	public static void sendTo(BMMessage base,EntityPlayerMP player){
		network.sendTo(new BMMessage.MessageOverride(base), player);
	}

	public static void sendToAll(BMMessage base) {
		network.sendToAll(new BMMessage.MessageOverride(base));
	}

	
	public static BMMessage create(int id) 
		throws InstantiationException, IllegalAccessException{
			return registeredMessages.get(id).newInstance();
	}

}
