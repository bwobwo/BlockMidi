package bwo.blockmidi.core;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;

/**
 * Just the utility class
 * @author BwoBwo
 */
public class BMUtil {
	public static float clamp(float val, float min, float max){
		return Math.min(Math.max(val,min),max);
	}
	public static int clamp(int val,int min,int max){
		return Math.min(Math.max(val, min),max);
	}

	public static float tryParseFloat(String s, int errorCode){
		try{
			return Float.parseFloat(s);
		} catch(Exception e){
			return errorCode;
		}
	}
	
	public static int tryParse(String s,int errorCode){
		try{
			return Integer.parseInt(s);			
		} catch(Exception e){
			return errorCode;
		}
	}
	
	public static final BlockPos[] NEIGHBOURS = {
		new BlockPos(1,0,0),
		new BlockPos(-1,0,0),
		new BlockPos(0,1,0),
		new BlockPos(0,-1,0),
		new BlockPos(0,0,1),
		new BlockPos(0,0,-1)
	};
	
	//I think just object comparison is bugged or something? idk
	public static boolean isInArray(String[] array, String obj){
		for(String o: array){
			if(o.equals(obj)) return true;
		}
		return false;		
	}
	
	public static IBlockState[] getNeighbours(World world, BlockPos pos){
		IBlockState[] states = new IBlockState[NEIGHBOURS.length];
		for(int i=0;i<NEIGHBOURS.length;i++){
			states[i] = world.getBlockState(pos.add(NEIGHBOURS[i]));
		}
		return states;
	}
	
}
