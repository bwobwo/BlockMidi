package bwo.blockmidi.core;

import bwo.blockmidi.network.SFSync;
import bwo.blockmidi.synth.SongPlayer;
import bwo.blockmidi.synth.TickTimer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BMEventHandler {
	long lastTime=-1;
	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void render(TickEvent.RenderTickEvent evt){
		float dt = lastTime==-1?0:System.currentTimeMillis()-lastTime;
		lastTime = System.currentTimeMillis();
		SongPlayer.update(dt);
		TickTimer.update(dt);
	}
	
	@SubscribeEvent
	public void playerJoin(PlayerLoggedInEvent evt){
		BMNetwork.sendTo(new SFSync(BMCommon.gameSoundbankMap),
			(EntityPlayerMP)evt.player);
	}
}
