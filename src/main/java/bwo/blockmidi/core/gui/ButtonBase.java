package bwo.blockmidi.core.gui;

import net.minecraft.client.gui.GuiButton;

public class ButtonBase extends GuiButton{

	public static boolean includes(String v, String[] arr){
		if(arr.length==0) return true;
		for(String s: arr){
			if(s.equals(v)) return true;
		}
		return false;
	}
	
	String[] possible;
	
	public ButtonBase(int numId, int x,int y, int w, int h,String def, String... vals){
		super(numId,x,y,w,h,includes(def,vals)?def:vals[0]);
		this.possible=vals;
	}
	
	public void onClick(){
		for(int i=0;i<this.possible.length;i++){
			if(this.displayString.equals(this.possible[i])){
				this.displayString = this.possible[(i+1)%this.possible.length];
				return;
			}
		}
	}
}
