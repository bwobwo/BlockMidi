package bwo.blockmidi.core.gui;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiLabel;

public class LabelBase extends GuiLabel{

	public LabelBase(FontRenderer fontRendererObj, int p_i45540_2_, int p_i45540_3_, int p_i45540_4_, int p_i45540_5_,
			int p_i45540_6_, int p_i45540_7_) {
		super(fontRendererObj, p_i45540_2_, p_i45540_3_, p_i45540_4_, p_i45540_5_, p_i45540_6_, p_i45540_7_);
		this.fr=fontRendererObj;
	}
	
	public void setText(String text){
		this.addLine(text);
		this.text=text;
		this.width = fr.getStringWidth(this.text);
	}
	
	protected FontRenderer fr;
	public int width=0;
	public String text="";
}
