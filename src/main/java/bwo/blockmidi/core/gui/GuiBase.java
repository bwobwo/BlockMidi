package bwo.blockmidi.core.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bwo.blockmidi.core.BMDataBlock;
import bwo.blockmidi.core.NBTSettings;
import bwo.blockmidi.core.BMDataBlock.MusicTE;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiLabel;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * Simple GuiScreen with some shortcuts for music block TE values. 
 * @see BMDataBlock
 * @author BwoBwo
 */
public abstract class GuiBase extends GuiScreen{
	public World world;
	public BlockPos pos;
	public MusicTE te;
	public EntityPlayer player;
	public NBTSettings data;
	long starttime;
	protected int runningId=0;
	public static final int DEF_BTN_WIDTH = 200,DEF_BTN_HEIGHT = 20;
	 
	HashMap<String,Gui> guis = new HashMap<String,Gui>();
	List<GuiTextField> textFields = new ArrayList<GuiTextField>();
	
	List<ScrollableList> lists = new ArrayList<ScrollableList>();
	
	public GuiBase(World world, BlockPos pos,EntityPlayer player){
		
		this.world=world;
		this.pos=pos;
		this.player=player;
		
		this.starttime = System.currentTimeMillis();
		TileEntity te = world.getTileEntity(pos);
		if(te!=null&&(te instanceof MusicTE)){
			this.te = (MusicTE)world.getTileEntity(pos);
			this.data = this.te.getData();
		}
	}

	public ScrollableList addScrollList(ScrollableList list){
		this.lists.add(list);
		return list;
	}
	
	public ScrollableList addScrollList(int x, int y, int w, int h){
		return this.addScrollList(new ScrollableList(w,h,y,this.height-y,x,20,this));		
	}
	
	@Override
	public void actionPerformed(GuiButton btn){
		if(btn instanceof ButtonBase){
			((ButtonBase) btn).onClick();
		}
		this.buttonClicked(btn);
	}
	
	public void buttonClicked(GuiButton btn){
	}
	
	public ButtonBase addButtonSelection(String name,int y,String defText, String...possible){		
		ButtonBase base = new ButtonBase(this.runningId,this.width/2-DEF_BTN_WIDTH/2,y,DEF_BTN_WIDTH,DEF_BTN_HEIGHT,defText,possible);
		this.buttonList.add(base);
		this.guis.put(name, base);
		this.runningId++;
		return base;
	}
	
	public GuiButton addButton(String name, int x, int y, int w, int h,Object text){
		GuiButton btn = new GuiButton(this.runningId,x,y,w,h,""+text);
		this.buttonList.add(btn);
		this.guis.put(name,btn);
		this.runningId++;
		return btn;
	}
	
	public GuiButton addButton(String name,int y,Object text){
		GuiButton btn = new GuiButton(this.runningId,this.width/2-DEF_BTN_WIDTH/2,y,DEF_BTN_WIDTH,DEF_BTN_HEIGHT,""+text);
		this.buttonList.add(btn);
		this.guis.put(name,btn);
		this.runningId++;
		return btn;
	}
	
	public SliderBase addSlider(String name, int y, SliderBase base){
		base.x = (int)(this.width/2)-(base.width/2);
		this.addButton(base);
		base.y=y;
		this.guis.put(name, base);
		this.runningId++;
		return base;
	}
	
	public SliderBase addSlider(String name,String display,int y, int min, int max,int def,String... ops){
		SliderBase b = SliderBase.create(this.runningId, display,0, y, min, max, def, ops);
		b.x =(int) (this.width/2)-(b.width/2);
		this.addButton(b);
		this.guis.put(name, b);
		this.runningId++;
		return b;
	}
	
	public GuiTextField addTextInput(String name,int y,Object text){		
		GuiTextField field = new GuiTextField(this.runningId,this.fontRenderer,this.width/2-DEF_BTN_WIDTH/2,y,DEF_BTN_WIDTH,DEF_BTN_HEIGHT);
		field.setMaxStringLength(10);
		field.setText(""+text);
		this.runningId++;
		this.guis.put(name, field);
		this.textFields.add(field);
		return field;
	}
		
	public GuiTextField addLabeledInput(String name, String display, int y, Object text){
		return this.addLabeledInput(name, display, y, DEF_BTN_WIDTH,text);
	}
	
	public GuiTextField addLabeledInput(String name,String display, int y, int w, Object text){
		GuiTextField field = this.addTextInput(name, y, text);
		LabelBase base = this.addLabel(name+"_label", 0, 0, 0xFFFFFF, display);

		base.x = middleX()-w/2;
		base.y=y;
		field.x = middleX()-w/2+base.width+5;
		field.width = w-base.width-5;
		field.y = y;
		return field;
	}
	
	public int middleY(){
		return this.height/2;
	}
	public int middleX(){
		return this.width/2;
	}
	
	public LabelBase addLabel(String name,int x, int y, int color,String text){
		LabelBase label = new LabelBase(this.fontRenderer,this.runningId,x,y,200,20,0xFFFFFF);
		label.setText(text);		
		this.labelList.add(label);
		this.runningId++;
		this.guis.put(name, label);
		return label;
	}
	
	public FontRenderer getFontRenderer(){
		return this.fontRenderer;
	}
	
	protected void keyTyped(char par1, int par2) throws IOException
    {
        super.keyTyped(par1, par2);
        for(GuiTextField field: this.textFields){
			field.textboxKeyTyped(par1, par2);
		}
    }
	
	protected void mouseClicked(int x, int y, int btn) throws IOException{
		super.mouseClicked(x, y, btn);
		for(GuiTextField field: this.textFields){
			field.mouseClicked(x, y, btn);
		}
	}
	
	public void updateScreen(){
		super.updateScreen();
		for(GuiTextField field: this.textFields){
			field.updateCursorCounter();
		}
	}	
	
	
	public GuiLabel getLabel(String name){
		return (GuiLabel) this.guis.get(name);
	}
	
	public String getButtonValue(String name){
		return this.getButton(name).displayString;
	}
	
	public String getTextValue(String name){
		return this.getText(name).getText();
	}
	
	public GuiButton getButton(String name){
		return (GuiButton)this.guis.get(name);
	}
	
	public SliderBase getSlider(String name){
		return (SliderBase)this.guis.get(name);
	}
	public GuiTextField getText(String name){
		return (GuiTextField)this.guis.get(name);
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
	    this.drawDefaultBackground();
	    super.drawScreen(mouseX, mouseY, partialTicks);   
	    for(GuiTextField f: this.textFields){
	    	f.drawTextBox();
	    }
	    for(ScrollableList ls: this.lists){
	        ls.drawScreen(mouseX, mouseY, partialTicks);
	    }
	}
	
	public abstract void onClosed();
	
	
	/*
	 * TODO: Please for the love of god. 
	 * I don't know why it happens, so this is a workaround.
	 * 
	 * Problem: Gui fires twice directly. 
	 * 
	 * (non-Javadoc)
	 * @see net.minecraft.client.gui.GuiScreen#onGuiClosed()
	 */
	@Override
	public void onGuiClosed(){
		if(System.currentTimeMillis()-this.starttime<300) return;
		this.onClosed();
	}
	
	public void initGui(){
		super.initGui();
		this.labelList.clear();
	}
	
	@Override
	public boolean doesGuiPauseGame() {
	    return false;
	}
}
