package bwo.blockmidi.core.gui;

import net.minecraft.client.gui.GuiPageButtonList;
import net.minecraft.client.gui.GuiSlider;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class SliderBase extends GuiSlider{
	World world;
	BlockPos basePos;
	String display;
	String[] maps;
	
	public SliderBase(Helper helper, int id,float in){
		super(helper,id,0,0,"",0,1,in,helper);
	}
	
	/**
	 * Call SettingSlider.create(...)
	 */
	public SliderBase(Helper helper, int idIn, String display,int x, int y, int low, int high,float initValue,String[] maps) {
		super(helper, idIn, x, y, "fra", low, high, initValue, helper);
		helper.slider = this;
		this.maps=maps;
		this.display=display;
		this.setSliderValue(this.getSliderValue(), false);
	}
		
	public void valueChanged(float value){	
	}
	
	public static SliderBase create(int id, String display,int x, int y, int low, int high,int initValue,String... maps){
		Helper h = new Helper();
		return new SliderBase(h,id,display,x,y,low,high,initValue,maps);
	}
	
	public float getValue(){
		return this.getSliderValue();
	}
	
	public String getText(int id, String name, float value){
		for(String s: maps){
			try{
				String[] parts = s.split(":");
				int v = Integer.parseInt(parts[0]);
				if(v==(int)value){
					parts[0] = "";
					return display+": "+String.join("", parts);
				}
			} catch(Exception e){
			}
		}			
		return display+": "+(int)value;
	}
	
	public void setEntryValue(int id, float value){
		setSliderValue((int)value, false);
		valueChanged(getSliderValue());
	}
	
	
	public static class Helper implements GuiPageButtonList.GuiResponder, GuiSlider.FormatHelper{
		public SliderBase slider;
		
		@Override
		public String getText(int id, String name, float value) {
			if(slider==null) return "";
			return slider.getText(id, name, value);
		}
		
		@Override
		public void setEntryValue(int id, float value) {
			if(slider==null) return;
			slider.setEntryValue(id, value);
		}
		public void setEntryValue(int id, boolean value) {}
		public void setEntryValue(int id, String value) {}
	}
}
