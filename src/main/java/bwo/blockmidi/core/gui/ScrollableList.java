package bwo.blockmidi.core.gui;

import java.util.LinkedHashMap;
import java.util.Map;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraftforge.fml.client.GuiScrollingList;

public class ScrollableList extends GuiScrollingList{

	public LinkedHashMap<String,Object> valueMap = new LinkedHashMap<String,Object>();
	
	public String getKeyAt(int index) {
	    for (Map.Entry<String,Object> entry : valueMap.entrySet()) {
	        if (index-- == 0) {
	            return entry.getKey();
	        }
	    }
	    return null;
	}
	
	public int selectedIndex;
	GuiBase parent;
	
	public ScrollableList(int x, int y, int w, int h, GuiBase parent){
		super(Minecraft.getMinecraft(),w,h,y,parent.height-y,x,20,parent.width,parent.height);
		this.parent=parent;
	}
	
	public ScrollableList(int width, int height, int top, int bottom, int left, int entryHeight, GuiBase parent) {
		super(Minecraft.getMinecraft(), width, height, top, bottom, left, entryHeight,parent.width,parent.height);
		this.parent=parent;
	}

	
	public ScrollableList add(String s, Object obj){
		valueMap.put(s,obj);
		return this;
	}
	
	public Object get(String s){
		return valueMap.get(s);
	}
		
	public ScrollableList add(String s){
		valueMap.put(s,null);
		return this;
	}
	
	public void clear(){
		valueMap.clear();
	}
	
	public Object getSelectedValue(){
		try{
			return this.get(this.getKeyAt(this.selectedIndex));			
		} catch(Exception e){
			return null;
		}
	}
	
	@Override
	protected int getSize() {
		return valueMap.keySet().size();
	}

	@Override
	protected void elementClicked(int index, boolean doubleClick) {
		selectedIndex=index;
		this.onSelection(index);
	}
	
	public void onSelection(int index){
		
	}

	@Override
	protected boolean isSelected(int index) {
		return index==selectedIndex;
	}

	@Override
	protected void drawBackground() {
	}

    @Override
    protected void drawSlot(int idx, int right, int top, int height, Tessellator tess)
    {
    	FontRenderer font = parent.getFontRenderer();
    	font.drawString(font.trimStringToWidth(getKeyAt(idx),listWidth - 10), this.left + 3 , top +  2, 0xFF2222);	
    }
}