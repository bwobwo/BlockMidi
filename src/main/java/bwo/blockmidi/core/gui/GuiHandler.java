package bwo.blockmidi.core.gui;

import bwo.blockmidi.core.BMBlockBase;
import bwo.blockmidi.core.BMMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

/**
 * @author BwoBwo
 */
public class GuiHandler implements IGuiHandler {

	/**
	 * Loads both client and server gui (if registered)
	 * 
	 * @param name
	 * @param player
	 * @param pos
	 */
	public static void openGui(String name, EntityPlayer player, BlockPos pos) {
		player.openGui(BMMod.INSTANCE, -1, player.getEntityWorld(), pos.getX(), pos.getY(), pos.getZ());
	}
	
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		try{
			return ((BMBlockBase)world.getBlockState(new BlockPos(x,y,z)).getBlock()).serverGui(world,new BlockPos(x,y,z),player);
		} catch(Exception err){
			return null;			
		}
	}
	
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		try{
			return ((BMBlockBase)world.getBlockState(new BlockPos(x,y,z)).getBlock()).makeGui(world,new BlockPos(x,y,z),player);
		} catch(Exception err){
			err.printStackTrace();
			return null;
		}
	}
}