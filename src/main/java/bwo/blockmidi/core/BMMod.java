package bwo.blockmidi.core;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

/**
 * Game data registry is done through {@link BMRegistryHandler}
 * @author BwoBwo
 */
@Mod(modid = BMMod.MODID, version = BMMod.VERSION)
public class BMMod {
	public static final String MODID = "blockmidi";
	public static final String VERSION = "1.0";
	public static BMMod INSTANCE;

	@SidedProxy(clientSide="bwo.blockmidi.core.BMClient",
		serverSide="bwo.blockmidi.core.BMServer")
	public static BMCommon common;
		
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		BMMod.INSTANCE = this;
		common.preInit(event);
	}
	
	@EventHandler
	public void init(FMLInitializationEvent event) {
		common.init(event);
	}	
	
	@EventHandler
	public void serverStarting(FMLServerStartingEvent event){
		common.gameStarting(event);
	}
}
