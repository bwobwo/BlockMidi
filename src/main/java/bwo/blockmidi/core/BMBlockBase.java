package bwo.blockmidi.core;

import bwo.blockmidi.core.gui.GuiHandler;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * The base class for all music-related blocks. 
 * @author BwoBwo
 */
public class BMBlockBase extends Block{
	public ItemBlock ib;
	public String name;
	public BMBlockBase(String name) {
		super(Material.GROUND);
		this.ib = new ItemBlock(this);
		this.name = name;
		this.setRegistryName("seq_" + name);
		this.setUnlocalizedName("seq_" + name);
		this.ib.setRegistryName("seq_" + name);
		this.ib.setUnlocalizedName("seq_" + name);
		this.setCreativeTab(BMCommon.tab);
		this.ib.setCreativeTab(BMCommon.tab);
		BMRegistryHandler.registerBlock(this);
		BMRegistryHandler.registerItem(this.ib);
		BMRegistryHandler.registerModel(this.name, this.ib);
	}
	
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn,
			EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {		
		GuiHandler.openGui(this.name, playerIn,pos);
		return false;
	}	
	
	@SideOnly(Side.CLIENT)
	public Object makeGui(World world,BlockPos pos, EntityPlayer player){
		return null;
	}
	
	public Object serverGui(World world, BlockPos pos, EntityPlayer player){
		return null;
	}
	
}
