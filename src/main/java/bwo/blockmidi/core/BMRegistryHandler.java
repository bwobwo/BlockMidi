package bwo.blockmidi.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;

/**
 * I'm not quite comfortable doing the new registry list yet,
 * So this is a wrapper around that system. 
 * 
 * @author BwoBwo
 */
public class BMRegistryHandler {
	private static List<Block> registeredBlocks = new ArrayList<Block>();
	private static List<Item> registeredItems = new ArrayList<Item>();
	static HashMap<String, Item> itemModels = new HashMap<String, Item>();

	public static void registerTileEntity(String name,Class<? extends TileEntity> entity){
		GameRegistry.registerTileEntity(entity, name);
	}

	public static void registerItem(Item i) {
		registeredItems.add(i);
	}

	public static void registerModel(String s, Item i) {
		itemModels.put(s, i);
	}

	public static void registerBlock(Block b) {
		registeredBlocks.add(b);
	}

	@Mod.EventBusSubscriber
	public static class Handler {
		@SubscribeEvent
		public static void itemRegistry(RegistryEvent.Register<Item> event) {
			for (Item i : registeredItems) {
				event.getRegistry().register(i);
			}

			if (FMLCommonHandler.instance().getEffectiveSide().equals(Side.CLIENT)) {
				for (Entry<String, Item> er : itemModels.entrySet()) {
					ModelLoader.setCustomModelResourceLocation(er.getValue(), 0,
							new ModelResourceLocation(BMMod.MODID + er.getKey()));
				}
			} else {
				itemModels = null;
			}
			
		}

		@SubscribeEvent
		public static void blockRegistry(RegistryEvent.Register<Block> event) {
			for (Block b : registeredBlocks) {
				event.getRegistry().register(b);
			}
		}
	}
}
