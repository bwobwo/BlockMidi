package bwo.blockmidi.core;

import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;

public class BMServer extends BMCommon{
	public void preInit(FMLPreInitializationEvent evt){
		effectiveSide = Side.SERVER;
		super.preInit(evt);
	}
}
