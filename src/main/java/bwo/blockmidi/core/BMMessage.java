package bwo.blockmidi.core;

import java.nio.charset.StandardCharsets;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * Merged IMessage/IMessageHandler. 
 * Used in {@link BMNetwork}
 * @author BwoBwo
 */
public abstract class BMMessage {
	
	public abstract void fromBytes(ByteBuf buf);
	public abstract void toBytes(ByteBuf buf);
	
	public IMessage onClientMessage(NetHandlerPlayClient ctx){return null;}
	public IMessage onServerMessage(NetHandlerPlayServer ctx){return null;}
	
	public void writeString(ByteBuf b,String s){
		b.writeInt(s.length());
		b.writeCharSequence(s,StandardCharsets.UTF_8);
	}
	
	public String readString(ByteBuf b){
		return b.readCharSequence(b.readInt(), StandardCharsets.UTF_8).toString();
	}
	
	public MessageOverride getMessage(){
		return new MessageOverride(this);
	}
	
	public int getId(){
		return BMNetwork.registeredMessages.indexOf(this.getClass());
	}
	
	public static class MessageOverride implements IMessage{
		BMMessage msg;
		ByteBuf buf;
		public MessageOverride(BMMessage msg){
			this.msg=msg;
		}
		
		public MessageOverride(){
			
		}
		
		@Override
		public void fromBytes(ByteBuf buf) {

			int no = buf.readInt();
			try {
				this.msg = BMNetwork.create(no);
				this.msg.fromBytes(buf);
			} catch (Exception e) {
				//people can easily forge these, just ignore errors here. 
				e.printStackTrace();
			}
		}
		
		@Override
		public void toBytes(ByteBuf buf) {

			buf.writeInt(msg.getId());
			this.msg.toBytes(buf);
		}	
	}
	public static class MessageHandlerOverride implements IMessageHandler<MessageOverride,IMessage>{
		@Override
		public IMessage onMessage(MessageOverride message, MessageContext ctx) {
			if(message.msg==null) return null;
			if(ctx.side.isClient()){
				return message.msg.onClientMessage(ctx.getClientHandler());
			} 
			return message.msg.onServerMessage(ctx.getServerHandler());
		}
	}
}
