package bwo.blockmidi.core;

import java.io.File;
import java.io.IOException;
import java.util.List;

import bwo.blockmidi.blocks.InstrumentBlock;
import bwo.blockmidi.blocks.InstrumentItem;
import bwo.blockmidi.blocks.InstrumentMaker;
import bwo.blockmidi.blocks.NoteBlock;
import bwo.blockmidi.blocks.SettingsBlock;
import bwo.blockmidi.blocks.StartBlock;
import bwo.blockmidi.blocks.TimeSignature;
import bwo.blockmidi.config.ConfigLoader;
import bwo.blockmidi.core.gui.GuiHandler;
import bwo.blockmidi.network.MBlockSync;
import bwo.blockmidi.network.MakeInstrument;
import bwo.blockmidi.network.SFSync;
import bwo.blockmidi.synth.IDirProvider;
import bwo.blockmidi.synth.SoundfontManager;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.relauncher.Side;

public class BMCommon {
	public static InstrumentItem instrumentItem;

	public static Side effectiveSide;
	
	public static List<String> gameSoundbankMap;
		
	//We only use one creative tab. 
	public static CreativeTabs tab = new CreativeTabs("blockmidi_creative") {
		@Override
		public ItemStack getTabIconItem() {
			return new ItemStack(Items.APPLE);
		}
	};
	
	public void preInit(FMLPreInitializationEvent event) {
		SoundfontManager.provider = new IDirProvider(){
			@Override
			public File getSoundfontDirInt() {
				return DimensionManager.getCurrentSaveRootDirectory().getParentFile();
			}

			@Override
			public File getSaveDirInt() {
				return DimensionManager.getCurrentSaveRootDirectory();
			}
		};
		
		ConfigLoader.load();
		MinecraftForge.EVENT_BUS.register(new BMEventHandler());
		instrumentItem = new InstrumentItem();
		
		new InstrumentBlock();
		new InstrumentMaker();
		new SettingsBlock();
		new NoteBlock();
		new TimeSignature();
		new StartBlock();
		
		BMNetwork.init();
		BMNetwork.register(SFSync.class, false, true);
		BMNetwork.register(MBlockSync.class, true, false);
		BMNetwork.register(MakeInstrument.class, true, false);
		
	}
	
	public void init(FMLInitializationEvent event) {
		NetworkRegistry.INSTANCE.registerGuiHandler(BMMod.INSTANCE, new GuiHandler());
	}

	public void gameStarting(FMLServerStartingEvent event) {
		try {
			gameSoundbankMap = SoundfontManager.load(effectiveSide.isClient());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
