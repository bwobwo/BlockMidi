package bwo.blockmidi.core;

import java.util.HashMap;

import net.minecraft.nbt.NBTTagCompound;

/**
 * NBT wrapper that only allows edits conforming to a 'prototype'
 * @author BwoBwo
 */
public class NBTPrototype extends NBTTagCompound{
	String blockType;
	
	public HashMap<String,String[]> enums = new HashMap<String,String[]>();
	
	public void setEnum(String k, String... values){
		enums.put(k, values);
		this.setByte(k, 0);
	}

	public byte enumIndex(String k, String v){
		String[] s = enums.get(k);
		for(byte i=0;i<s.length;i++){
			if(s[i].equalsIgnoreCase(v)) return (byte)i;
		}
		return (byte)-1;
	}
	
	public String[] getEnumValues(String k){
		return this.enums.get(k);
	}
	public String getEnum(String k, byte b){
		return enums.get(k)[b];
	}
	public String getEnum(String k){		
		return enums.get(k)[0]; //always this.
	}
	
	public void setByte(String k, double b){
		this.setByte(k, (byte)(int)b);
	}
	public void setInt(String k, double v){
		this.setInteger(k, (int)v);
	}
	public void setShort(String k, short v){
		this.setShort(k,v);
	}
	public void setLong(String k,long v){
		this.setLong(k,v);
	}
	
	public String getBlockType() {
		return this.blockType;
	}
	
	public void setBlockType(String bt){
		System.out.println("BlockType set: "+bt);
		this.blockType=bt;
	}
}
