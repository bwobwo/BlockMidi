package bwo.blockmidi.tests;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.junit.Test;

import bwo.blockmidi.synth.IDirProvider;
import bwo.blockmidi.synth.SoundfontManager;

public class SoundfontRegistry {
	static void delete(File f) throws IOException {
		  if (f.isDirectory()) {
		    for (File c : f.listFiles())
		      delete(c);
		  }
		  if (!f.delete())
		    throw new FileNotFoundException("Failed to delete file: " + f);
		}

	
	@Test
	public void test() throws IOException {
		File tmp = new File("test");
		tmp.mkdirs();
		System.out.println(tmp.getAbsolutePath());
		try{
			SoundfontManager.provider = new IDirProvider(){
				@Override
				public File getSoundfontDirInt() {
					return tmp;
				}
				@Override
				public File getSaveDirInt() {
					return tmp;
				}
			};
		} finally{
			delete(tmp);
		}
	}
}
