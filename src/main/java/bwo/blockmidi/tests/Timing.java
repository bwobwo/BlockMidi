package bwo.blockmidi.tests;

import org.junit.Test;

import bwo.blockmidi.synth.Cursor;

public class Timing {
	@Test
	public void test() {
		Cursor c = new Cursor();
		c.bpm=60;
		c.beatsPerBar=4;
		c.beatUnit=4;
		assert(c.beatToMs(1)==1000);
		assert(c.barToMs(1)==4000);
		assert(c.noteToMs(2, 4)==2000);

		c.bpm=120;
		c.beatsPerBar=3;
		c.beatUnit=3;
		assert(c.beatToMs(1)==500);
		assert(c.barToMs(1)==1500);
		assert(c.noteToMs(1, 3)==500);		
	}
}
