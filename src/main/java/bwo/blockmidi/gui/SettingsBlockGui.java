package bwo.blockmidi.gui;

import bwo.blockmidi.core.gui.GuiBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class SettingsBlockGui extends GuiBase{
	public SettingsBlockGui(World world, BlockPos pos, EntityPlayer player) {
		super(world, pos, player);
	}

	public void initGui(){
		this.addSlider("attack","Attack",100,-1,127,data.getByte("attack"),"-1:Not set");
		this.addSlider("decay","Decay",125,-1,127,data.getByte("decay"),"-1:Not set");
		this.addSlider("velocity","Velocity",150,-1,127,data.getByte("velocity"),"-1:Not set");
	}

	@Override
	public void onClosed() {
		this.data.setByte("attack", this.getSlider("attack").getValue());
		this.data.setByte("decay", this.getSlider("decay").getValue());
		this.data.setByte("velocity", this.getSlider("velocity").getValue());
		this.data.sendUpdate();				
	}
}
