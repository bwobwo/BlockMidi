package bwo.blockmidi.gui;

import java.util.List;

import javax.sound.midi.Instrument;
import javax.sound.midi.Soundbank;

import bwo.blockmidi.blocks.InstrumentMaker;
import bwo.blockmidi.core.BMClient;
import bwo.blockmidi.core.BMCommon;
import bwo.blockmidi.core.BMNetwork;
import bwo.blockmidi.core.gui.GuiBase;
import bwo.blockmidi.core.gui.ScrollableList;
import bwo.blockmidi.synth.SoundfontManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class InstrumentSelector extends GuiBase{
	public InstrumentSelector(World world, BlockPos pos, EntityPlayer player) {
		super(world, pos, player);
	}
	
	ScrollableList soundbanks, instruments;
	
	@Override
	public void initGui(){
		//Soundfonts
		soundbanks = this.addScrollList(new ScrollableList(100, 50, 100, 100,this){
			public void onSelection(int sel){
				
			}
		});
		
		instruments = this.addScrollList(200, 50, 100, 100);
		
		this.addButtonSelection("nameSetting",10,"Registry Names",
			"Registry Names","Default Names");
		
		loadSoundfonts();
	}
	
	public void loadSoundfonts(){
		List<Soundbank> sbs = SoundfontManager.getAllSoundbanks(BMClient.clientSoundbankMap);
		soundbanks.clear();

		if(sbs.size()<=0){
			soundbanks.add("No soundbanks :(");
			return;
		}

		for(Soundbank b: sbs){
			soundbanks.add(b.getName(),b);
		}
		loadInstruments();
	}

	public void loadInstruments(){
		instruments.clear();
		Object b = soundbanks.getSelectedValue();
		if(b==null||(!(b instanceof Soundbank))) return;
		Soundbank soundbank = (Soundbank) b;
		for(Instrument instrument: soundbank.getInstruments()){
			instruments.add(instrument.getName(),instrument);
		}
	}

	@Override
	public void onClosed() {
		BMNetwork.sendToServer(null);
	}
}