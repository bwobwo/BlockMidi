package bwo.blockmidi.gui;

import bwo.blockmidi.blocks.NoteBlock;
import bwo.blockmidi.core.BMUtil;
import bwo.blockmidi.core.gui.GuiBase;
import bwo.blockmidi.core.gui.LabelBase;
import bwo.blockmidi.core.gui.SliderBase;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class NoteBlockGui extends GuiBase{
	public NoteBlockGui(World world, BlockPos pos, EntityPlayer player) {
		super(world, pos, player);
	}

	GuiButton selectedBtn;

	GuiTextField sleepField2;
	LabelBase divLabel;

	final String[] noteNames = new String[]{"C","C#","D","D#","E","F","F#","G","G#","A","A#","B"};
	
	float lastOctave=-1;
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		super.drawScreen(mouseX, mouseY, partialTicks);
	
		float v = this.getSlider("octave").getValue();
		if(v!=lastOctave){
			for(int i=0;i<=127;i++){
				GuiButton btn = this.getButton(""+i);
				btn.x =(int) (this.width/2+(i-127*v)*20);
//				if(btn.x<83||btn.x>240) btn.visible=false;
//				else btn.visible=true;
			}
		}
		lastOctave=v;
	}
	
	@Override
	public void initGui(){
		for(int i=0;i<=127;i++){						
			this.addButton(""+i,this.width+(i-64)*20,100,20,20,noteNames[i%12]);
//			if(btn.x<83||btn.x>240) btn.visible=false;
		}				
		
		this.addSlider("octave", 80, new SliderBase(new SliderBase.Helper(),
			this.runningId++,"",0,0,0,1,0,new String[]{}){
			public void setEntryValue(int id, float value){
				setSliderValue(value, false);
			}
		});
		
		this.addButtonSelection("sleepMode",160,data.getEnum("sleepMode"),
			data.getEnumValues("sleepMode"));

		//Time entry
		LabelBase label = this.addLabel("sleep_label",100,200,0xFFFFFF,"Sleep Time");
		GuiTextField f1 = this.addTextInput("sleep1",200,"");
		sleepField2 = this.addTextInput("sleep2", 200, "");
		divLabel = this.addLabel("divSign", 100, 200, 0xFFFFFF, "/");
		
		label.x=100;
		f1.x = label.x+label.width+5;
		f1.width = 50;
		divLabel.x = f1.x+f1.width+5;
		sleepField2.x = divLabel.x+divLabel.width+5;
		sleepField2.width = 50;
		
		//This is only sensible to set once. 
		float f = data.getFloat("sleep");
		if(noteSleepMode()){
			f1.setText(""+NoteBlock.getBeats(f));
			sleepField2.setText(""+NoteBlock.getBars(f));
		} else {
			f1.setText(""+f);
		}
		
		//PassThrough
		this.addButtonSelection("pass", 180, data.getBool("pass")?
				"Pass Through: On":"Pass Through: Off", 
				"Pass Through: On","Pass Through: Off");
		this.buttonClicked(this.getButton(""+this.data.getInt("note")));
	}
	
	public boolean noteSleepMode(){
		return this.getButtonValue("sleepMode").equalsIgnoreCase("Notes");
	}
	
	public void hideDivSign(){
		if(noteSleepMode()){
			sleepField2.setVisible(true);
			divLabel.visible=true;
		} else {
			divLabel.visible=false;
			sleepField2.setVisible(false);		
		}
	}
	
	@Override
	public void buttonClicked(GuiButton btn){
		hideDivSign();
		if(btn==null){
			System.out.println("null btn");
			return; //? 
		}
		System.out.println("Clicking button with id: "+btn.id);
		if(btn.id<128){
			if(this.selectedBtn!=null){
				this.selectedBtn.enabled=true;
			}
			this.selectedBtn=btn;
			btn.enabled=false;					
			return;					
		}
	}
	
	@Override
	public void onClosed() {
		int key = 6*12;
		if(this.selectedBtn!=null){
			key=this.selectedBtn.id;			
		}
		
		System.out.println("Key is now: ");
		
		this.data.setByte("note",key);
		this.data.setEnum("sleepMode",this.getButtonValue("sleepMode"));
		
		this.data.setBoolean("pass",
			this.getButton("pass").displayString.equals("Pass Through: On"));
		
		if(noteSleepMode()){
			int first = BMUtil.tryParse(this.getText("sleep1").getText(),0);
			int second = BMUtil.tryParse(this.getText("sleep2").getText(), 0);
			int tot = first<<16+second;					
			this.data.setFloat("sleep",Float.intBitsToFloat(tot));
		} else {
			this.data.setFloat("sleep",BMUtil.tryParseFloat(this.getText("sleep1").getText(),0));
		}
		this.data.sendUpdate();
	}
}
