package bwo.blockmidi.gui;

import bwo.blockmidi.blocks.InstrumentBlock.InstrumentInventoryTE;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.IInventory;

public class InstrumentContainerGui extends GuiContainer {
	public InstrumentContainerGui(IInventory playerInv, InstrumentInventoryTE te) {
		super(new InstrumentContainer(playerInv, te));
		this.xSize = 176;
		this.ySize = 166;
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
	}
}
