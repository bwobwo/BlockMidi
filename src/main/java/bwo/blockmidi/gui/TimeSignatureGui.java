package bwo.blockmidi.gui;

import bwo.blockmidi.core.BMUtil;
import bwo.blockmidi.core.gui.GuiBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class TimeSignatureGui extends GuiBase{
	public TimeSignatureGui(World world, BlockPos pos, EntityPlayer player) {
		super(world, pos, player);
	}

	public void initGui(){				
		this.addSlider("beatUnit","Beat Unit",100,-1,127,data.getByte("beats"),"-1:Not set");
		this.addSlider("beatsPerBar","Beats Per Bar",125,-1,127,data.getByte("bars"),"-1:Not set");
		int bpm = data.getInt("bpm");
		this.addLabeledInput("bpm","BPM",150,150,bpm<=0?"Not set":bpm);
	}

	@Override
	public void onClosed() {
		this.data.setByte("beats", this.getSlider("beats").getValue());
		this.data.setByte("bars", this.getSlider("bars").getValue());
		this.data.setInt("bpm", BMUtil.clamp(BMUtil.tryParse(this.getText("bpm").getText(),0),0,999));
		this.data.sendUpdate();  
	}
}
