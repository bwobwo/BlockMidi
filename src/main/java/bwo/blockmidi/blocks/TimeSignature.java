package bwo.blockmidi.blocks;

import bwo.blockmidi.core.BMDataBlock;
import bwo.blockmidi.core.NBTPrototype;
import bwo.blockmidi.gui.InstrumentSelector;
import bwo.blockmidi.gui.TimeSignatureGui;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * Notation settings. 
 * @author BwoBwo
 */
public class TimeSignature extends BMDataBlock{
	public TimeSignature() {
		super("time_signature");
	}
	@Override
	protected NBTPrototype getDataTemplate(NBTPrototype blank) {
		blank.setByte("beatUnit",(byte)-1);
		blank.setByte("beatsPerBar",(byte)-1);
		blank.setInteger("bpm", 0);
		return blank;
	}
	@Override
	public Object makeGui(World world, BlockPos pos, EntityPlayer player) {
		return new InstrumentSelector(world,pos,player);
	}
}
