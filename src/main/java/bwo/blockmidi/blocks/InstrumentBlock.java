package bwo.blockmidi.blocks;

import bwo.blockmidi.core.BMBlockBase;
import bwo.blockmidi.core.BMRegistryHandler;
import bwo.blockmidi.gui.InstrumentContainer;
import bwo.blockmidi.gui.InstrumentContainerGui;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Container block for {@link InstrumentItem}
 * Uses a single-slot container for players to store an instrument. 
 * @author BwoBwo
 */
public class InstrumentBlock extends BMBlockBase implements ITileEntityProvider{
	public InstrumentBlock() {
		super("instrument");
		BMRegistryHandler.registerTileEntity("instrument_inventory_te",InstrumentInventoryTE.class);
	}
	
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new InstrumentInventoryTE();
	}	
	
	public Object serverGui(World world, BlockPos pos,EntityPlayer player){
		TileEntity te = world.getTileEntity(pos);
		if(te instanceof InstrumentInventoryTE){
			return new InstrumentContainer(player.inventory,(InstrumentInventoryTE)te);			
		}
		return null;		
	}

	@SideOnly(Side.CLIENT)
	public Object makeGui(World world, BlockPos pos, EntityPlayer player){
		TileEntity te = world.getTileEntity(pos);
		if(te instanceof InstrumentInventoryTE){
			return new InstrumentContainerGui(player.inventory,(InstrumentInventoryTE)te);			
		}		
		return null;
	}
	
	
	/**
	 * Implements the Tile Entity inventory method found at 
	 * {@link <a href="https://bedrockminer.jimdo.com/modding-tutorials/advanced-modding/tile-entity-with-inventory/">Bedrock - Tile Entity with Inventory</a>}
	 */
	public static class InstrumentInventoryTE extends TileEntity implements IInventory {
		ItemStack stack = ItemStack.EMPTY;
		
		public ItemStack getItem(){
			return stack;
		}
		
		public InstrumentInventoryTE(){
		}
		
		@Override
		public void readFromNBT(NBTTagCompound compound){
			super.readFromNBT(compound);
			this.stack = new ItemStack(compound.getCompoundTag("item"));
		}
		@Override
		public NBTTagCompound writeToNBT(NBTTagCompound compound){	
			super.writeToNBT(compound);			
			compound.setTag("item",stack.serializeNBT());
			return compound;
		}
		
		@Override
		public String getName() {
			return "instrument_te";
		}

		@Override
		public boolean hasCustomName() {
			return true;
		}

		@Override
		public int getSizeInventory() {
			return 1;
		}

		@Override
		public boolean isEmpty() {
			return this.stack==null;
		}

		@Override
		public ItemStack getStackInSlot(int index) {
			return this.stack!=null?this.stack:ItemStack.EMPTY;
		}

		@Override
		public ItemStack decrStackSize(int index, int count) {
			if (this.getStackInSlot(index) != null) {
		        ItemStack itemstack;

		        if (this.getStackInSlot(index).getCount() <= count) {
		            itemstack = this.getStackInSlot(index);
		            this.setInventorySlotContents(index, null);
		            this.markDirty();
		            return itemstack;
		        } else {
		            itemstack = this.getStackInSlot(index).splitStack(count);

		            if (this.getStackInSlot(index).getCount() <= 0) {
		                this.setInventorySlotContents(index, null);
		            } else {
		                //Just to show that changes happened
		                this.setInventorySlotContents(index, this.getStackInSlot(index));
		            }

		            this.markDirty();
		            return itemstack;
		        }
		    } else {
		        return null;
		    }			
		}

		@Override
		public ItemStack removeStackFromSlot(int index) {
			return null;
		}
	
		@Override
		public void setInventorySlotContents(int index, ItemStack stack) {
		    if (index < 0 || index >= this.getSizeInventory())
		        return;
		    if (stack != null && stack.getCount() > this.getInventoryStackLimit())
		        stack.setCount(this.getInventoryStackLimit());
		    if (stack != null && stack.getCount()== 0)
		        stack = null;
		    this.stack = stack;
		    this.markDirty();
		}

		@Override
		public int getInventoryStackLimit() {
			return 1;
		}

		@Override
		public boolean isUsableByPlayer(EntityPlayer player) {
			return true;
		}

		@Override
		public void openInventory(EntityPlayer player) {
			
		}

		@Override
		public void closeInventory(EntityPlayer player) {
			
		}

		@Override
		public boolean isItemValidForSlot(int index, ItemStack stack) {
			return stack.getItem() instanceof InstrumentItem;
		}

		@Override
		public int getField(int id) {
			return 0;
		}

		@Override
		public void setField(int id, int value) {
		}

		@Override
		public int getFieldCount() {
			return 0;
		}

		@Override
		public void clear() {
			this.stack=null;
		}
	}

}
