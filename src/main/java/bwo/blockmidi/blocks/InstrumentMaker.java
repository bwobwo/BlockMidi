package bwo.blockmidi.blocks;

import bwo.blockmidi.core.BMBlockBase;
import bwo.blockmidi.gui.InstrumentSelector;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class InstrumentMaker extends BMBlockBase{
	public InstrumentMaker() {
		super("instrument_maker");		
	}

	@Override
	public Object makeGui(World world, BlockPos pos, EntityPlayer player) {
		return new InstrumentSelector(world,pos,player);
	}
}
