package bwo.blockmidi.blocks;

import bwo.blockmidi.core.BMDataBlock;
import bwo.blockmidi.core.NBTPrototype;
import bwo.blockmidi.gui.NoteBlockGui;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * @author BwoBwo
 */
public class NoteBlock extends BMDataBlock{
	public NoteBlock() {
		super("note_block");
	}
	
	public static int getBeats(float inV){
		return Float.floatToIntBits(inV)>>16;
	}
	public static int getBars(float inV){
		return Float.floatToIntBits(inV)&16;
	}
	
	@Override
	public Object makeGui(World world, BlockPos pos,EntityPlayer player) {
		return new NoteBlockGui(world,pos,player);
	}
	
	@Override
	protected NBTPrototype getDataTemplate(NBTPrototype blank) {
		blank.setByte("note", (byte)(12*6));
		blank.setEnum("sleepMode","Notes","Beats","Bars","Milliseconds");		
		blank.setBoolean("pass", false);
		blank.setFloat("sleep",0);		
		return blank;
	}
}