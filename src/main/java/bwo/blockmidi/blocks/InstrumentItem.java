package bwo.blockmidi.blocks;

import bwo.blockmidi.core.BMCommon;
import bwo.blockmidi.core.BMRegistryHandler;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

/**
 * Single item registry entry representing all loaded instruments. 
 * 
 * This design makes it possible (not implemented) to reload soundfonts at runtime.
 * 
 * The individual instrument is stored in the {@link ItemStack} damage value as such:  
 * Nibbles 1-4 : Font
 * Nibbles 5-7 : Program
 * Nibble  8   : Bank
 * 
 * @author BwoBwo
 */
public class InstrumentItem extends Item{
	public InstrumentItem(){
		super();
		this.setUnlocalizedName("instrument_item");
		this.setRegistryName("instrument_item");
		this.setCreativeTab(BMCommon.tab);
		BMRegistryHandler.registerItem(this);
	}
	
	
	//        Font            Program    Bank
	//------------------- -------------- ----
	//0000 0000 0000 0000 0000 0000 0000 0000
	
	
	public static int getSoundfont(ItemStack stack){
		return stack.getItemDamage()>>16&0xFFFF;
	}
	
	public static int getBank(ItemStack stack){
		return stack.getItemDamage()&0xF;
	}
	
	public static int getProgram(ItemStack stack){
		return stack.getItemDamage()>>4&0xFFF;
	}
	
	public static int getDamage(int soundfont, int bank, int program){
		return soundfont<<16+program<<4+bank;
	}
	
	public static void setDamage(ItemStack stack, int soundfont, int bank, int program){
		stack.setItemDamage(getDamage(soundfont,bank,program));
	}
	
	
	@Override
	public String getUnlocalizedName(ItemStack stack)
    {
		return getSoundfont(stack)+" "+getBank(stack)+" "+getProgram(stack);
    }
}
