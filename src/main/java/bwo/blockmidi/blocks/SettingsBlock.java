package bwo.blockmidi.blocks;

import bwo.blockmidi.core.BMDataBlock;
import bwo.blockmidi.core.NBTPrototype;
import bwo.blockmidi.gui.SettingsBlockGui;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * Controls attack, decay and velocity. 
 * I'm not yet sure how to implement sustain/release.
 * @author BwoBwo
 */
public class SettingsBlock extends BMDataBlock{
	public SettingsBlock() {
		super("settings");
	}
	
	@Override
	protected NBTPrototype getDataTemplate(NBTPrototype blank) {
		blank.setByte("attack", (byte)-1);
		blank.setByte("decay", (byte)-1);
		blank.setByte("velocity", (byte)-1);		
		return blank;
	}

	@Override
	public Object makeGui(World world, BlockPos pos, EntityPlayer player) {
		return new SettingsBlockGui(world,pos,player);
	}
}
