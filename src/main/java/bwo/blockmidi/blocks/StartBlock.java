package bwo.blockmidi.blocks;

import bwo.blockmidi.core.BMBlockBase;
import bwo.blockmidi.synth.SongPlayer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

/**
 * Used to play a song!
 * @author BwoBwo
 */
public class StartBlock extends BMBlockBase{
	public StartBlock() {
		super("start");
	}
	
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn,
			EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {		
		if(playerIn instanceof EntityPlayerSP){
			new SongPlayer(worldIn,pos,0,500,100);
		}
		
		Chunk c = worldIn.getChunkFromBlockCoords(new BlockPos(0,0,0));
		
	
		
		return false;
	}
}
