package bwo.blockmidi.network;

import java.nio.charset.StandardCharsets;

import bwo.blockmidi.core.BMMessage;
import bwo.blockmidi.core.BMDataBlock.MusicTE;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

/**
 * Used to sync client MusicBlock settings to the servers TileEntity.
 * @author BwoBwo
 */
public class MBlockSync extends BMMessage{
	NBTTagCompound updateCompound;	
	int x,y,z;
	public MBlockSync(BlockPos pos, NBTTagCompound compound){
		this.x=pos.getX();
		this.y=pos.getY();
		this.z=pos.getZ();
		
		this.updateCompound=compound;
	}
	
	public MBlockSync(){
		
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		x = buf.readInt();
		y = buf.readInt();
		z = buf.readInt();
		this.updateCompound = new NBTTagCompound();
		int c = buf.readByte();
		for(int i=0;i<c;i++){
			String key = buf.readCharSequence(
				buf.readByte(), StandardCharsets.UTF_8).toString();
			int id = buf.readByte();
			switch(id){
				case 1:
					updateCompound.setByte(key,buf.readByte());
					break;
				case 2:
					updateCompound.setShort(key,buf.readShort());
					break;
				case 3:
					updateCompound.setInteger(key,buf.readInt());
					break;
				case 4:
					updateCompound.setLong(key, buf.readInt());
					break;
				case 5:
					updateCompound.setFloat(key, buf.readFloat());
					break;
				case 6:
					updateCompound.setDouble(key, buf.readDouble());
					break;
				case 7:
					updateCompound.setByteArray(key, buf.readBytes(buf.readInt()).array());
				case 8:
					updateCompound.setString(key, buf.readCharSequence(
						buf.readByte(),StandardCharsets.UTF_8).toString());
			}
		}
	}
	
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(x);
		buf.writeInt(y);
		buf.writeInt(z);
		buf.writeByte(this.updateCompound.getSize());	
		for(String s: updateCompound.getKeySet()){
			buf.writeByte(s.length());
			buf.writeCharSequence(s, StandardCharsets.UTF_8);
			NBTBase b = updateCompound.getTag(s);
			buf.writeByte(b.getId());
			switch(b.getId()){
				case 1:
					buf.writeByte(updateCompound.getByte(s));
					break;
				case 2:
					buf.writeShort(updateCompound.getShort(s));
					break;
				case 3:
					buf.writeInt(updateCompound.getInteger(s));
					break;
				case 4:
					buf.writeLong(updateCompound.getLong(s));
					break;
				case 5:
					buf.writeFloat(updateCompound.getFloat(s));
					break;
				case 6:
					buf.writeDouble(updateCompound.getDouble(s));
					break;
				case 7:
					buf.writeInt(updateCompound.getByteArray(s).length);
					buf.writeBytes(updateCompound.getByteArray(s));
					break;
				case 8:
					buf.writeByte(updateCompound.getString(s).length());
					buf.writeCharSequence(updateCompound.getString(s), StandardCharsets.UTF_8);
			}
		}
	}

	@Override
	public IMessage onClientMessage(NetHandlerPlayClient ctx) {
		return null;
	}
	@Override
	public IMessage onServerMessage(NetHandlerPlayServer ctx) {
		System.out.println("Server receiving message");
		World w = ctx.player.getEntityWorld();
		TileEntity te = w.getTileEntity(new BlockPos(this.x,this.y,this.z));
		if(!(te instanceof MusicTE)) return null;
		MusicTE mte = (MusicTE) te;			
		
		
		
		mte.clientUpdate(this.updateCompound);
		return null;
	}

}
