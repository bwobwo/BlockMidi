package bwo.blockmidi.network;

import bwo.blockmidi.core.BMCommon;
import bwo.blockmidi.core.BMMessage;
import io.netty.buffer.ByteBuf;
import net.minecraft.item.ItemStack;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

/**
 * 
 * @author BwoBwo
 *
 */
public class MakeInstrument extends BMMessage{
	int dmg;
	
	public MakeInstrument(){
		
	}
	
	public MakeInstrument(int dmg){
		this.dmg=dmg;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		this.dmg=buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(this.dmg);
	}
	
	public IMessage onServerMessage(NetHandlerPlayServer ctx){
		ItemStack st = new ItemStack(BMCommon.instrumentItem);
		st.setCount(1);
		st.setItemDamage(this.dmg);
		ctx.player.addItemStackToInventory(st);
		return null;
	}

}
