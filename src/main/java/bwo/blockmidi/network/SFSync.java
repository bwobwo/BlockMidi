package bwo.blockmidi.network;

import java.util.ArrayList;
import java.util.List;

import bwo.blockmidi.core.BMClient;
import bwo.blockmidi.core.BMMessage;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

/**
 * Used to sync a game servers soundfont mappings with players. 
 * Sent whenever a player joins a server (including single player),
 * or when the server reloads them. 
 * 
 * @author BwoBwo
 *
 */
public class SFSync extends BMMessage{
	List<String> mapping;
	
	public SFSync(){}

	public SFSync(List<String> mapping){
		this.mapping=mapping;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		int c = buf.readInt();
		
		this.mapping = new ArrayList<String>();
		
		for(int i=0;i<c;i++){
			mapping.add(this.readString(buf));
		}
	}
	
	@Override
	public void toBytes(ByteBuf buf) {		
		buf.writeInt(mapping.size());
		for(String s: mapping){
			writeString(buf,s);
		}
	}
	
	public IMessage onClientMessage(NetHandlerPlayClient ctx){
		BMClient.clientSoundbankMap = this.mapping;
		return null;
	}
	
	public IMessage onServerMessage(NetHandlerPlayServer ctx){
		return null;
	}

}
